<?php
/**
 * @file
 * Functions to generate ctools layouts.
 */

/**
 * Get the regions for the layout.
 *
 * If the Drush command was issued with the regions option then the regions
 * supplied is returned as an array. If no region option was supplied, the array
 * contains top, center, bottom.
 *
 * @return array
 *   The regions as an array.
 */
function plugin_stub_get_regions() {
  $options = drush_get_option('regions', FALSE);
  if ($options != FALSE) {
    return _convert_csv_to_array($options);
  }
  return array('top', 'center', 'bottom');
}

function plugin_stub_generate_plugin_layout($plugin_name, $prefix) {
  $regions = plugin_stub_get_regions();
  $regions_arr = array();
  foreach ($regions as $region) {
    $regions_arr[] = "\t\t'$region' => t('" . ucfirst($region) . "'),";
  }
  $regions_string = implode("\n", $regions_arr);
  return <<<CONTENT
<?php

/**
 * @file
 * layout plugin generated by drush ctools plugin stub.
 */

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
\$plugin = array(
  'title' => t('NAME OF YOUR LAYOYT'),
  'category' => t('CATEGORY OF YOUR LAYOUT. Eg. Colums: 1'),
  'icon' => 'ctools_stub.png',
  'theme' => '{$prefix}',
  'css' => '{$plugin_name}.css',
  'regions' => array(
{$regions_string}
  ),
);
CONTENT;
}

/**
 * Finalize the plugin.
 *
 * Panels likes it when there is an icon for content types, so give the plugin
 * a default icon.
 */
function plugin_stub_finalize_layout($plugin_name, $filename) {
  plugin_stub_copy_icon($plugin_name, $filename);

  $pathinfo = pathinfo($filename);
  $dst_path_and_filename = drush_cwd() . '/' . $pathinfo['dirname'] . '/' . $plugin_name;

  $regions = plugin_stub_get_regions();
  $divs = array();
  $classes = array();
  foreach ($regions as $region) {
    $divs[] = "\t\t<div class=\"$region\"><?php print \$content['$region']; ?></div>";
    $classes[] = ".panel-{$plugin_name} .{$region} {\n\t/* Your CSS here */\n}";
  }
  $divs = implode("\n", $divs);
  $classes = implode("\n", $classes);

  $tpl_content = <<<TPL
<?php
/**
 * @file
 * Template for {$plugin_name}
 */
?>
<div class="panel-display panel-{$plugin_name}" <?php if (!empty(\$css_id)) { print "id=\"\$css_id\""; } ?>>
{$divs}
</div>
TPL;
  plugin_stub_write($dst_path_and_filename . '.tpl.php', $tpl_content);

  $css_content = <<<CSS
.panel-{$plugin_name} {
  /* Your CSS here */
}
{$classes}
CSS;
  plugin_stub_write($dst_path_and_filename . '.css', $css_content);
}
